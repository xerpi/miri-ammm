'''
AMMM Instance Generator v1.0
Instance Generator class.
Copyright 2016 Luis Velasco and Lluis Gifre.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import os, random

# Generate instances based on read configuration.
class InstanceGenerator(object):
    def __init__(self, config):
        self.config = config

    def generate(self):
        instancesDirectory = self.config.instancesDirectory
        fileNamePrefix = self.config.fileNamePrefix
        fileNameExtension = self.config.fileNameExtension
        numInstances = self.config.numInstances

        minWorkersRequired = self.config.minWorkersRequired
        maxWorkersRequired = self.config.maxWorkersRequired
        minCost_1 = self.config.minCost_1
        maxCost_1 = self.config.maxCost_1
        minCost_2 = self.config.minCost_2
        maxCost_2 = self.config.maxCost_2
        minCost_3 = self.config.minCost_3
        maxCost_3 = self.config.maxCost_3
        numProviders = self.config.numProviders
        minAvailableWorkers = self.config.minAvailableWorkers
        maxAvailableWorkers = self.config.maxAvailableWorkers
        minCostContract = self.config.minCostContract
        maxCostContract = self.config.maxCostContract
        minCountries = self.config.minCountries
        maxCountries = self.config.maxCountries
        minCostWorker = self.config.minCostWorker
        maxCostWorker = self.config.maxCostWorker

        if(not os.path.isdir(instancesDirectory)):
            raise Exception('Directory(%s) does not exist' % instancesDirectory)

        for i in xrange(0, numInstances):
            instancePath = os.path.join(instancesDirectory, '%s_%d.%s' % (fileNamePrefix, i, fileNameExtension))
            fInstance = open(instancePath, 'w')

            soluble = False

            while not soluble:
                numCountries = random.randint(minCountries, maxCountries);

                numWorkers = 0
                availableWorkers = []
                costContract = []
                countries = []
                for p in xrange(0, numProviders):
                    numWorkersPerProvider = random.randint(minAvailableWorkers, maxAvailableWorkers)
                    availableWorkers.append(numWorkersPerProvider)
                    numWorkers += numWorkersPerProvider
                    costContractPerProvider = random.uniform(minCostContract, maxCostContract)
                    costContract.append(costContractPerProvider)
                    countryPerProvider = random.randint(0, numCountries)
                    countries.append(countryPerProvider)
                    costWorkerPerProvider = random.uniform(minCostWorker, maxCostWorker)
                    costWorker.append(costWorkerPerProvider)

                cost_1 = random.uniform(minCost_1, maxCost_1)
                cost_2 = random.uniform(minCost_2, maxCost_2)
                cost_3 = random.uniform(minCost_3, maxCost_3)

                if numWorkers >= minWorkersRequired and numWorkers <= maxWorkersRequired:
                    soluble = True
                    workersRequired = random.randint(minWorkersRequired, numWorkers)

            fInstance.write('wr=%d;\n' % numWorkers)
            fInstance.write('cost_1=%d;\n' % cost_1)
            fInstance.write('cost_2=%d;\n' % cost_2)
            fInstance.write('cost_3=%d;\n' % cost_3)
            fInstance.write('nProviders=%d;\n' % numProviders)
            fInstance.write('available_workers=[%s];\n' % (' '.join(map(str, availableWorkers))))
            fInstance.write('cost_contract=[%s];\n' % (' '.join(map(str, costContract))))
            fInstance.write('country=[%s];\n' % (' '.join(map(str, countries))))
            fInstance.write('cost_worker=[%s];\n' % (' '.join(map(str, costWorker))))

            fInstance.close()
