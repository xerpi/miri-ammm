'''
AMMM Instance Generator v1.0
Config attributes validator.
Copyright 2016 Luis Velasco and Lluis Gifre.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

# Validate config attributes read from a DAT file.
class ValidateConfig(object):
    @staticmethod
    def validate(data):
        # Validate that mandatory input parameters were found
        for paramName in ['instancesDirectory', 'fileNamePrefix', 'fileNameExtension', 'numInstances',
                          'minWorkersRequired', 'maxWorkersRequired', 'minCost_1', 'maxCost_1', 'minCost_2',
                          'maxCost_2', 'minCost_3', 'maxCost_3', 'nProviders', 'minAvailableWorkers',
                          'maxAvailableWorkers', 'minCostContract', 'maxCostContract', 'minCountries',
                          'maxCountries', 'minCostWorker', 'maxCostWorker']:
            if(not data.__dict__.has_key(paramName)):
                raise Exception('Parameter(%s) not contained in Configuration' % str(paramName))

        instancesDirectory = data.instancesDirectory
        if(len(instancesDirectory) == 0): raise Exception('Value for instancesDirectory is empty')

        fileNamePrefix = data.fileNamePrefix
        if(len(fileNamePrefix) == 0): raise Exception('Value for fileNamePrefix is empty')

        fileNameExtension = data.fileNameExtension
        if(len(fileNameExtension) == 0): raise Exception('Value for fileNameExtension is empty')

        numInstances = data.numInstances
        if(not isinstance(numInstances, (int, long)) or (numInstances <= 0)):
            raise Exception('numInstances(%s) has to be a positive integer value.' % str(numInstances))

        minWorkersRequired = data.minWorkersRequired
        if(not isinstance(minWorkersRequired, (int, long)) or (minWorkersRequired <= 0)):
            raise Exception('minWorkersRequired(%s) has to be a positive integer value.' % str(minWorkersRequired))

        maxWorkersRequired = data.maxWorkersRequired
        if(not isinstance(maxWorkersRequired, (int, long)) or (maxWorkersRequired <= 0) or (maxWorkersRequired < minWorkersRequired)):
            raise Exception('maxWorkersRequired(%s) has to be a positive integer value.' % str(maxWorkersRequired))

        minCost_1 = data.minCost_1
        if(not isinstance(minCost_1, (int, long)) or (minCost_1 <= 0)):
            raise Exception('minCost_1(%s) has to be a positive integer value.' % str(minCost_1))

        maxCost_1 = data.maxCost_1
        if(not isinstance(maxCost_1, (int, long)) or (maxCost_1 <= 0) or (maxCost_1 < minCost_1)):
            raise Exception('maxCost_1(%s) has to be a positive integer value, and bigger than %s.' % str(maxCost_1), str(minCost_1))

        minCost_2 = data.minCost_2
        if(not isinstance(minCost_2, (int, long)) or (minCost_2 <= 0)):
            raise Exception('minCost_2(%s) has to be a positive integer value.' % str(minCost_2))

        maxCost_2 = data.maxCost_2
        if(not isinstance(maxCost_2, (int, long)) or (maxCost_2 <= 0) or (maxCost_2 < minCost_2)):
            raise Exception('maxCost_2(%s) has to be a positive integer value.' % str(maxCost_2))

        minCost_3 = data.minCost_3
        if(not isinstance(minCost_3, (int, long)) or (minCost_3 <= 0)):
            raise Exception('minCost_3(%s) has to be a positive integer value.' % str(minCost_3))

        maxCost_3 = data.maxCost_3
        if(not isinstance(maxCost_3, (int, long)) or (maxCost_3 <= 0) or (maxCost_3 < minCost_3)):
            raise Exception('maxCost_3(%s) has to be a positive integer value.' % str(maxCost_3))

        numProviders = data.nProviders
        if(not isinstance(numProviders, (int, long)) or (numProviders <= 0)):
            raise Exception('numProviders(%s) has to be a positive integer value.' % str(numProviders))

        minAvailableWorkers = data.minAvailableWorkers
        if(not isinstance(minAvailableWorkers, (int, long)) or (minAvailableWorkers <= 0)):
            raise Exception('minAvailableWorkers(%s) has to be a positive integer value.' % str(minAvailableWorkers))
        if minAvailableWorkers % 2 == 1:
            raise Exception('minAvailableWorkers(%s) needs to be an even number' % minAvailableWorkers)

        maxAvailableWorkers = data.maxAvailableWorkers
        if(not isinstance(maxAvailableWorkers, (int, long)) or (maxAvailableWorkers <= 0) or (maxAvailableWorkers < minAvailableWorkers)):
            raise Exception('maxAvailableWorkers(%s) has to be a positive integer value.' % str(maxAvailableWorkers))
        if maxAvailableWorkers % 2 == 1:
                raise Exception('maxAvailableWorkers(%s) needs to be an even number' % maxAvailableWorkers)

        minCostContract = data.minCostContract
        if(not isinstance(minCostContract, (int, long)) or (minCostContract <= 0)):
            raise Exception('minCostContract(%s) has to be a positive integer value.' % str(minCostContract))

        maxCostContract = data.maxCostContract
        if(not isinstance(maxCostContract, (int, long)) or (maxCostContract <= 0) or (maxCostContract < minCostContract)):
            raise Exception('maxCostContract(%s) has to be a positive integer value.' % str(maxCostContract))

        minCountries = data.minCountries
        if(not isinstance(minCountries, (int, long)) or (minCountries <= 0)):
            raise Exception('minCountries(%s) has to be a positive integer value.' % str(minCountries))

        maxCountries = data.maxCountries
        if(not isinstance(maxCountries, (int, long)) or (maxCountries <= 0) or (maxCountries < minCountries)):
            raise Exception('maxCountries(%s) has to be a positive integer value.' % str(maxCountries))

        minCostWorker = data.minCostWorker
        if(not isinstance(minCostWorker, (int, long)) or (minCostWorker <= 0)):
            raise Exception('minCostWorker(%s) has to be a positive integer value.' % str(minCostWorker))

        maxCostWorker = data.maxCostWorker
        if(not isinstance(maxCostWorker, (int, long)) or (maxCostWorker <= 0) or (maxCostWorker <= minCostWorker)):
            raise Exception('maxCostWorker(%s) has to be a positive integer value.' % str(maxCostWorker))
