#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>
#include "Local_Search.h"
#include "Solver_Greedy_LS.h"

Solver_Greedy_LS::Solver_Greedy_LS()
{

}

Solver_Greedy_LS::~Solver_Greedy_LS()
{

}

Solution *Solver_Greedy_LS::solve(std::shared_ptr<DAT> dat)
{
	Solution *solution_greedy = Solver_Greedy::solve(dat);
	if (!solution_greedy)
		return nullptr;

	Solution *solution_ls = localSearch(solution_greedy, dat);
	if (!solution_ls)
		return nullptr;

	if (solution_ls->getCost() < solution_greedy->getCost()) {
		delete solution_greedy;
		return solution_ls;
	} else {
		delete solution_ls;
		return solution_greedy;
	}
}
