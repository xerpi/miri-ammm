#ifndef SOLVER_GREEDY_H
#define SOLVER_GREEDY_H

#include <memory>
#include <list>
#include <utility>
#include "ISolver.h"
#include "Solution.h"
#include "DAT.h"

class Solver_Greedy : ISolver {
public:
	Solver_Greedy();
	~Solver_Greedy();
	Solution *solve(std::shared_ptr<DAT> dat);
};

#endif
