#ifndef SOLUTION_H
#define SOLUTION_H

#include <cinttypes>
#include <memory>
#include <list>
#include "DAT.h"

#define COST_1_THRESHOLD	5
#define COST_2_THRESHOLD	5

/*
 * A solution is:
 *   For each provider, whether we hire none, half or full,
 *   and if we hire full, how many extra we hire.
 */

class Solution {
public:
	enum candidate_type_t {
		CANDIDATE_TYPE_HIRE_HALF,
		CANDIDATE_TYPE_HIRE_FULL,
		CANDIDATE_TYPE_HIRE_EXTRA // Hire 1 extra worker
	};

	struct candidate_t {
		candidate_type_t type;
		uint64_t provider;
	};

	enum contract_type_t {
		CONTRACT_TYPE_HALF,
		CONTRACT_TYPE_FULL  // Might have extra
	};

	struct provider_contract_t {
		contract_type_t type;
		uint64_t provider;
		uint64_t num_extra;
	};

	typedef std::list<candidate_t> candidate_list_t;
	typedef std::pair<float, candidate_list_t::const_iterator> candidate_cost_pair_t;
	typedef std::vector<Solution::candidate_cost_pair_t> candidate_cost_pair_vector_t;

	Solution(std::shared_ptr<DAT> dat);
	~Solution();

	Solution *clone(void) const;

	uint64_t calculateProviderCost(uint64_t nworkers, uint64_t provider) const;
	uint64_t getCost() const;
	uint64_t getNumContractedWorkers(uint64_t provider) const;
	uint64_t getTotalNumContractedWorkers() const;
	bool isComplete() const;

	void getCandidateList(candidate_list_t &candidates) const;
	void calculateCandidatesCost(const std::list<candidate_t> &candidates, std::vector<candidate_cost_pair_t> &candidates_cost) const;
	float getCandidateCost(const candidate_t &candidate) const;
	void addCandidateToSolution(const candidate_t &candidate);

	void getProviderContractList(std::list<provider_contract_t> &contracts) const;
	bool getProviderContract(uint64_t provider, provider_contract_t &contract) const;
	void removeProviderContract(uint64_t provider);
	void addProviderContract(const provider_contract_t &contract);
	bool canHireExactWorkersFromProvider(uint64_t nworkers, uint64_t provider, provider_contract_t &out_contract) const;
	bool hiredAnotherProviderFromSameCountry(uint64_t provider) const;

	void print() const;
	void printCandidates(const candidate_list_t &candidates) const;
	void printCandidatesCost(const std::vector<candidate_cost_pair_t> &candidates) const;
	void printContract(provider_contract_t &contract) const;

private:
	bool hiredProviderFromCountry(uint64_t country) const;

	std::shared_ptr<DAT> dat;

	std::vector<bool> hired_half;
	std::vector<bool> hired_full;
	std::vector<uint64_t> hired_extra;
};

#endif
