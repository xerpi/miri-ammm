#ifndef DAT_H
#define DAT_H

#include <cinttypes>
#include <vector>
#include <string>

class DAT {
public:
	DAT();
	~DAT();
	bool loadFromFile(const char *filename);
	void print();

	uint64_t getWr() { return wr; };
	uint64_t getCost_1() { return cost_1; };
	uint64_t getCost_2() { return cost_2; };
	uint64_t getCost_3() { return cost_3; };
	uint64_t getNumProviders() { return nProviders; };
	uint64_t getAvailableWorkers(uint64_t provider) { return available_workers[provider]; };
	uint64_t getCostContract(uint64_t provider) { return cost_contract[provider]; };
	uint64_t getCountry(uint64_t provider) { return country[provider]; };
	uint64_t getCostWorker(uint64_t provider) { return cost_worker[provider]; };

private:
	static inline std::string &ltrim(std::string &s, const char *t = " \t\n\r\f\v")
	{
		s.erase(0, s.find_first_not_of(t));
		return s;
	}

	static inline std::string &rtrim(std::string &s, const char *t = " \t\n\r\f\v;")
	{
		s.erase(s.find_last_not_of(t) + 1);
		return s;
	}

	inline std::string& trim(std::string &s)
	{
		return ltrim(rtrim(s));
	}

	static void readVector(const std::string &line, std::vector<uint64_t> &v, uint64_t size);
	static void printVector(const std::vector<uint64_t> &v);

	uint64_t wr;
	uint64_t cost_1;
	uint64_t cost_2;
	uint64_t cost_3;
	uint64_t nProviders;
	std::vector<uint64_t> available_workers;
	std::vector<uint64_t> cost_contract;
	std::vector<uint64_t> country;
	std::vector<uint64_t> cost_worker;
};

#endif
