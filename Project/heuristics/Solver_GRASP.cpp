#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>
#include "Local_Search.h"
#include "Solver_GRASP.h"

std::random_device Solver_GRASP::rd;
std::mt19937 Solver_GRASP::gen(Solver_GRASP::rd());

Solver_GRASP::Solver_GRASP(float alpha, uint64_t maxIterations)
	: alpha(alpha), maxIterations(maxIterations)
{

}

Solver_GRASP::~Solver_GRASP()
{

}

Solution *Solver_GRASP::solve(std::shared_ptr<DAT> dat)
{
	Solution *bestSolution = nullptr;

	for (uint64_t i = 0; i < maxIterations; i++) {
		Solution *itBestSolution = nullptr;

		Solution *constrSolution = constructivePhase(dat);
		if (!constrSolution)
			continue;

		Solution *lsSolution = localSearch(constrSolution, dat);

		/* Get best solution among constructive phase and local search.
		 * (local search might fail) */
		if (lsSolution) {
			if (lsSolution->getCost() < constrSolution->getCost()) {
				itBestSolution = lsSolution;
				delete constrSolution;
			} else {
				itBestSolution = constrSolution;
				delete lsSolution;
			}
		} else {
			itBestSolution = constrSolution;
		}

		if (bestSolution) {
			if (itBestSolution->getCost() < bestSolution->getCost()) {
				delete bestSolution;
				bestSolution = itBestSolution;
			} else {
				delete itBestSolution;
			}
		} else {
			bestSolution = itBestSolution;
		}
	}

	return bestSolution;
}

Solution *Solver_GRASP::constructivePhase(std::shared_ptr<DAT> dat)
{
	Solution *solution = new Solution(dat);

	while (!solution->isComplete()) {
		// Get full list of candidates
		Solution::candidate_list_t candidates;
		solution->getCandidateList(candidates);
		if (candidates.empty()) {
			delete solution;
			return nullptr;
		}

		// Calculate the cost of the candidates
		Solution::candidate_cost_pair_vector_t candidates_cost;
		solution->calculateCandidatesCost(candidates, candidates_cost);
		// Check for feasible candidates
		if (candidates_cost.empty()) {
			delete solution;
			return nullptr;
		}

		// Sort the list of feasible candidates
		std::sort(candidates_cost.begin(), candidates_cost.end(),
			[&](Solution::candidate_cost_pair_t const& lhs, Solution::candidate_cost_pair_t const& rhs) {
				return lhs.first < rhs.first;
			}
		);

		// Get the candidates with minimum and maximum costs
		Solution::candidate_cost_pair_t &candidate_min = candidates_cost.front();
		Solution::candidate_cost_pair_t &candidate_max = candidates_cost.back();

		// Calculate the q_min threshold
		float qmin_cut = candidate_min.first + alpha * (candidate_max.first - candidate_min.first);

		// Restricted Candidate List
		std::vector<Solution::candidate_cost_pair_vector_t::const_iterator> RCL;

		// Add elements to the RCL
		for (auto it = candidates_cost.begin(); it != candidates_cost.end() && it->first <= qmin_cut; it++) {
			RCL.push_back(it);
		}

		// Pick a candidate from the RCL at random
		std::uniform_int_distribution<> dis(0, RCL.size() - 1);
		Solution::candidate_cost_pair_t candidate_sel = *RCL[dis(gen)];

		// Add it to the solution
		solution->addCandidateToSolution(*candidate_sel.second);
	}

	return solution;
}

void Solver_GRASP::setAlpha(float newAlpha)
{
	alpha = newAlpha;
}

float Solver_GRASP::getAlpha()
{
	return alpha;
}
