#ifndef LOCAL_SEARCH_H
#define LOCAL_SEARCH_H

#include "Solution.h"
#include "DAT.h"

Solution *localSearch(const Solution *origSolution, std::shared_ptr<DAT> dat);

#endif
