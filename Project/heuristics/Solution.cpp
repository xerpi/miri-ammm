#include <iostream>
#include <limits>
#include "Solution.h"

Solution::Solution(std::shared_ptr<DAT> dat) : dat(dat)
{
	hired_half.resize(dat->getNumProviders(), false);
	hired_full.resize(dat->getNumProviders(), false);
	hired_extra.resize(dat->getNumProviders(), 0);
}

Solution::~Solution()
{

}

Solution *Solution::clone(void) const
{
	Solution *newSolution = new Solution(dat);
	newSolution->hired_half.assign(hired_half.begin(), hired_half.end());
	newSolution->hired_full.assign(hired_full.begin(), hired_full.end());
	newSolution->hired_extra.assign(hired_extra.begin(), hired_extra.end());
	return newSolution;
}

uint64_t Solution::calculateProviderCost(uint64_t nworkers, uint64_t provider) const
{
	uint64_t cost = 0;

	if (nworkers > 0)
		cost += dat->getCostContract(provider);

	if (nworkers > COST_1_THRESHOLD) {
		uint64_t num_more_cost_1 = nworkers - COST_1_THRESHOLD;
		cost += COST_1_THRESHOLD * dat->getCost_1();
		if (num_more_cost_1 > COST_2_THRESHOLD) {
			uint64_t num_more_cost_2 = num_more_cost_1 - COST_2_THRESHOLD;
			cost += COST_2_THRESHOLD * dat->getCost_2();
			cost += num_more_cost_2 * dat->getCost_3();
		} else {
			cost += num_more_cost_1 * dat->getCost_2();
		}
	} else {
		cost += nworkers * dat->getCost_1();
	}

	cost += nworkers * dat->getCostWorker(provider);

	return cost;
}

uint64_t Solution::getCost() const
{
	uint64_t cost = 0;

	for (uint64_t i = 0; i < dat->getNumProviders(); i++) {
		uint64_t nworkers = getNumContractedWorkers(i);
		cost += calculateProviderCost(nworkers, i);
	}

	return cost;
}

uint64_t Solution::getNumContractedWorkers(uint64_t provider) const
{
	uint64_t n = 0;
	n += hired_full[provider] ? dat->getAvailableWorkers(provider) : 0;
	n += hired_half[provider] ? dat->getAvailableWorkers(provider) / 2: 0;
	n += hired_extra[provider];
	return n;
}

uint64_t Solution::getTotalNumContractedWorkers() const
{
	uint64_t n = 0;

	for (uint64_t i = 0; i < dat->getNumProviders(); i++)
		n += getNumContractedWorkers(i);

	return n;
}

bool Solution::isComplete() const
{
	return getTotalNumContractedWorkers() == dat->getWr();
}

void Solution::getCandidateList(std::list<candidate_t> &candidates) const
{
	for (uint64_t i = 0; i < dat->getNumProviders(); i++) {
		if (hired_full[i]) {
			if (hired_extra[i] < dat->getAvailableWorkers(i)) {
				candidates.push_back({CANDIDATE_TYPE_HIRE_EXTRA, i});
			}
		} else {
			if (hired_half[i])
				candidates.push_back({CANDIDATE_TYPE_HIRE_FULL, i});
			else
				candidates.push_back({CANDIDATE_TYPE_HIRE_HALF, i});
		}
	}
}

void Solution::calculateCandidatesCost(const std::list<candidate_t> &candidates, std::vector<candidate_cost_pair_t> &candidates_cost) const
{
	for (auto it = candidates.begin(); it != candidates.end(); it++) {
		float cost = getCandidateCost(*it);
		// Don't add into the candidate list if the cost is infinity (infeasible)
		if (cost != std::numeric_limits<float>::infinity()) {
			candidates_cost.push_back(std::make_pair(cost, it));
		}
	}
}

bool Solution::hiredProviderFromCountry(uint64_t country) const
{
	for (uint64_t i = 0; i < dat->getNumProviders(); i++) {
		if (hired_half[i] || hired_full[i]) {
			if (dat->getCountry(i) == country)
				return true;
		}
	}

	return false;
}

bool Solution::hiredAnotherProviderFromSameCountry(uint64_t provider) const
{
	uint64_t country = dat->getCountry(provider);
	for (uint64_t i = 0; i < dat->getNumProviders(); i++) {
		if (i == provider)
			continue;

		if (hired_half[i] || hired_full[i]) {
			if (dat->getCountry(i) == country)
				return true;
		}
	}

	return false;
}

float Solution::getCandidateCost(const candidate_t &candidate) const
{
	float cost = std::numeric_limits<float>::infinity();
	uint64_t provider = candidate.provider;
	uint64_t candidate_country = dat->getCountry(provider);
	uint64_t curTotalWorkers, curProviderWorkers, curProviderCost, newWorkers;

	/* Can't hire from two providers from the same country! */
	if ((candidate.type == CANDIDATE_TYPE_HIRE_HALF) &&
	    hiredProviderFromCountry(candidate_country))
		return cost;

	curProviderWorkers = getNumContractedWorkers(provider);
	curProviderCost = calculateProviderCost(curProviderWorkers, provider);
	curTotalWorkers = getTotalNumContractedWorkers();

	if (candidate.type == CANDIDATE_TYPE_HIRE_HALF)
		newWorkers = dat->getAvailableWorkers(provider) / 2;
	else if (candidate.type == CANDIDATE_TYPE_HIRE_FULL)
		newWorkers = dat->getAvailableWorkers(provider);
	else
		newWorkers = 1;

	if (curTotalWorkers + newWorkers <= dat->getWr()) {
		uint64_t newProviderWorkers = curProviderWorkers + newWorkers;
		uint64_t newProviderCost = calculateProviderCost(newProviderWorkers, provider);
		cost = (newProviderCost - curProviderCost) / float(newWorkers);
	}

	//if (candidate.type == CANDIDATE_TYPE_HIRE_EXTRA)
	//	Might want to add some extra cost here

	return cost;
}

void Solution::addCandidateToSolution(const candidate_t &candidate)
{
	if (candidate.type == CANDIDATE_TYPE_HIRE_HALF) {
		hired_half[candidate.provider] = true;
	} else if (candidate.type == CANDIDATE_TYPE_HIRE_FULL) {
		hired_half[candidate.provider] = false;
		hired_full[candidate.provider] = true;
	} else if (candidate.type == CANDIDATE_TYPE_HIRE_EXTRA) {
		hired_extra[candidate.provider]++;
	}
}

void Solution::getProviderContractList(std::list<provider_contract_t> &contracts) const
{
	for (uint64_t i = 0; i < dat->getNumProviders(); i++) {
		provider_contract_t contract;
		if (getProviderContract(i, contract))
			contracts.push_back(contract);
	}
}

bool Solution::getProviderContract(uint64_t provider, provider_contract_t &contract) const
{
	if (hired_half[provider] || hired_full[provider]) {
		contract.type = hired_half[provider] ? CONTRACT_TYPE_HALF : CONTRACT_TYPE_FULL;
		contract.provider = provider;
		contract.num_extra = hired_extra[provider];
		return true;
	}
	return false;
}

void Solution::removeProviderContract(uint64_t provider)
{
	hired_half[provider] = false;
	hired_full[provider] = false;
	hired_extra[provider] = 0;
}

void Solution::addProviderContract(const provider_contract_t &contract)
{
	hired_half[contract.provider] = contract.type == CONTRACT_TYPE_HALF;
	hired_full[contract.provider] = contract.type == CONTRACT_TYPE_FULL;
	hired_extra[contract.provider] = contract.num_extra;
}

bool Solution::canHireExactWorkersFromProvider(uint64_t nworkers, uint64_t provider, provider_contract_t &out_contract) const
{
	uint64_t available_workers = dat->getAvailableWorkers(provider);

	if (nworkers == available_workers / 2) {
		out_contract.type = CONTRACT_TYPE_HALF;
		out_contract.provider = provider;
		out_contract.num_extra = 0;
		return true;
	} else if ((nworkers >= available_workers) && (nworkers <= 2 * available_workers)) {
		out_contract.type = CONTRACT_TYPE_FULL;
		out_contract.provider = provider;
		out_contract.num_extra = nworkers - available_workers;
		return true;
	}

	return false;
}

void Solution::print() const
{
	std::cout << "Hired half: ";
	for (auto x: hired_half)
		std::cout << " " << x;
	std::cout << std::endl;

	std::cout << "Hired full: ";
	for (auto x: hired_full)
		std::cout << " " << x;
	std::cout << std::endl;

	std::cout << "Hired extra:";
	for (auto x: hired_extra)
		std::cout << " " << x;
	std::cout << std::endl;

	std::cout << "Cost: " << getCost() << std::endl;
}

void Solution::printCandidates(const candidate_list_t &candidates) const
{
	for (auto it = candidates.begin(); it != candidates.end(); it++) {
		std::cout << "Hire from provider " << it->provider << " ";
		if (it->type == CANDIDATE_TYPE_HIRE_HALF)
			std::cout << "(" << (dat->getAvailableWorkers(it->provider))/2 <<  ") half";
		else if(it->type == CANDIDATE_TYPE_HIRE_FULL)
			std::cout << "(" << dat->getAvailableWorkers(it->provider) <<  ") full";
		else
			std::cout << "(" << (dat->getAvailableWorkers(it->provider) + hired_extra[it->provider] + 1) << ") extra";
		std::cout << std::endl;
	}
}

void Solution::printCandidatesCost(const std::vector<candidate_cost_pair_t> &candidates) const
{
	for (auto it = candidates.begin(); it != candidates.end(); it++) {
		std::cout << "Hire from provider " << it->second->provider << " ";
		if (it->second->type == CANDIDATE_TYPE_HIRE_HALF)
			std::cout << "(" << (dat->getAvailableWorkers(it->second->provider))/2 <<  ") half";
		else if(it->second->type == CANDIDATE_TYPE_HIRE_FULL)
			std::cout << "(" << dat->getAvailableWorkers(it->second->provider) <<  ") full";
		else
		 	std::cout << "(" << (dat->getAvailableWorkers(it->second->provider) + hired_extra[it->second->provider] + 1) << ") extra";
		std::cout << " cost = " << it->first << std::endl;
	}
}

void Solution::printContract(provider_contract_t &contract) const
{
	uint64_t provider_workers = dat->getAvailableWorkers(contract.provider);
	uint64_t num_workers = (contract.type == CONTRACT_TYPE_HALF) ? (provider_workers / 2) :
		(provider_workers + contract.num_extra);

	std::cout << "Provider " << contract.provider <<  " ("
	          << (contract.type == CONTRACT_TYPE_HALF ? "half" : "full") << "), "
	          << "num workers: " << num_workers
	          << std::endl;
}
