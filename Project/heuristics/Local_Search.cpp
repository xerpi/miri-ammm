#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>
#include "Local_Search.h"

Solution *localSearch(const Solution *origSolution, std::shared_ptr<DAT> dat)
{
	Solution *solution = origSolution->clone();

	Solution::candidate_list_t candidates;
	solution->getCandidateList(candidates);

	std::list<Solution::provider_contract_t> contracts;
	solution->getProviderContractList(contracts);

	/* For each of the contracts in the solution */
	for (auto &contract: contracts) {
		uint64_t contract_nhired = solution->getNumContractedWorkers(contract.provider);

		uint64_t old_cost = solution->getCost();
		uint64_t best_cost = old_cost;
		struct {
			uint64_t old_contract_provider;
			bool candidate_had_prev_contract;
			Solution::provider_contract_t new_contract;
		} exchange;

		/* For each provider */
		for (uint64_t candidate = 0; candidate < dat->getNumProviders(); candidate++) {
			/* If the candidate is the same as the contract, skip */
			if (contract.provider == candidate)
				continue;

			/* If there's another provider from the same country
			 * as the candidate we want to hire, skip */
			if (solution->hiredAnotherProviderFromSameCountry(candidate))
				continue;

			uint64_t new_provider_workers_needed = solution->getNumContractedWorkers(candidate) + contract_nhired;
			Solution::provider_contract_t new_contract;
			if (solution->canHireExactWorkersFromProvider(new_provider_workers_needed, candidate, new_contract)) {
				/* Remove any possible old contract with the candidate provider */
				Solution::provider_contract_t candidate_prev_contract;
				bool candidate_had_prev_contract = solution->getProviderContract(candidate, candidate_prev_contract);

				/* Now check if less cost when changing candidate with contract */
				solution->removeProviderContract(contract.provider);
				if (candidate_had_prev_contract)
					solution->removeProviderContract(candidate_prev_contract.provider);
				solution->addProviderContract(new_contract);

				/* Get cost with the new contract */
				uint64_t new_cost = solution->getCost();

				/* Check if the new contract improves the solution cost */
				if (new_cost < best_cost) {
					best_cost = new_cost;
					exchange.old_contract_provider = contract.provider;
					exchange.candidate_had_prev_contract = candidate_had_prev_contract;
					exchange.new_contract = new_contract;
				}

				/* Put the old contracts back */
				solution->removeProviderContract(new_contract.provider);
				solution->addProviderContract(contract);
				if (candidate_had_prev_contract)
					solution->addProviderContract(candidate_prev_contract);
			}
		}

		/* If we have a new better cost, perform the exchange */
		if (best_cost < old_cost) {
			solution->removeProviderContract(exchange.old_contract_provider);
			if (exchange.candidate_had_prev_contract)
				solution->removeProviderContract(exchange.new_contract.provider);
			solution->addProviderContract(exchange.new_contract);
		}
	}

	if (!solution->isComplete()) {
		delete solution;
		return nullptr;
	}

	return solution;
}
