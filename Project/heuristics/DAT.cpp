#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cassert>
#include "DAT.h"

DAT::DAT()
{

}

DAT::~DAT()
{
	available_workers.clear();
	cost_contract.clear();
	country.clear();
	cost_worker.clear();
}

bool DAT::loadFromFile(const char *filename)
{
	std::string line;

	std::ifstream ifs(filename);
	if (!ifs.good())
		return false;

	while (std::getline(ifs, line)) {
		if (line[0] == '#')
			continue;

		std::istringstream is_line(line);
		std::string key;
		if (std::getline(is_line, key, '=')) {
			key = trim(key);
			std::string value;
			if (std::getline(is_line, value)) {
				value = trim(value);
				//std::cout << key << " : " << value << std::endl;

				if (key.compare("wr") == 0) {
					wr = std::stoull(value);
				} else if (key.compare("cost_1") == 0) {
					cost_1 = std::stoull(value);
				} else if (key.compare("cost_2") == 0) {
					cost_2 = std::stoull(value);
				} else if (key.compare("cost_3") == 0) {
					cost_3 = std::stoull(value);
				} else if (key.compare("nProviders") == 0) {
					nProviders = std::stoull(value);
					available_workers.resize(nProviders);
					cost_contract.resize(nProviders);
					country.resize(nProviders);
					cost_worker.resize(nProviders);
				} else if (key.compare("available_workers") == 0) {
					readVector(value, available_workers, nProviders);
					/* Check they are even numbers */
					for (auto &e: available_workers) {
						if (e % 2 != 0) {
							std::cout << "#available workers "
							          << e
							          << " is not an even number!"
							          << std::endl;
							return false;
						}
					}
				} else if (key.compare("cost_contract") == 0) {
					readVector(value, cost_contract, nProviders);
				} else if (key.compare("country") == 0) {
					readVector(value, country, nProviders);
				} else if (key.compare("cost_worker") == 0) {
					readVector(value, cost_worker, nProviders);
				}
			}
		}
	}

	return true;
}

void DAT::print()
{
	std::cout << "wr = " << wr << ";" << std::endl;
	std::cout << "cost_1 = " << cost_1 << ";" << std::endl;
	std::cout << "cost_2 = " << cost_2 << ";" << std::endl;
	std::cout << "cost_3 = " << cost_3 << ";" << std::endl;
	std::cout << "nProviders = " << nProviders << ";" << std::endl;
	std::cout << "available_workers = "; printVector(available_workers); std::cout << ";" << std::endl;
	std::cout << "cost_contract = "; printVector(cost_contract); std::cout << ";" << std::endl;
	std::cout << "country = "; printVector(country); std::cout << ";" << std::endl;
	std::cout << "cost_worker = "; printVector(cost_worker); std::cout << ";" << std::endl;
}

void DAT::readVector(const std::string &line, std::vector<uint64_t> &v, uint64_t size)
{
	uint64_t i = 0;
	// Remove [ and ]
	std::stringstream stream(line.substr(1, line.size() - 2));
	std::string word;
	while (std::getline(stream, word, ' ')) {
		std::stringstream wordss(word);
		std::string token;
		while (std::getline(wordss, token, ','))
			v[i++] = std::stoull(token);
	}
}

void DAT::printVector(const std::vector<uint64_t> &v)
{
	std::cout << "[";
	for (size_t i = 0; i < v.size(); i++) {
		std::cout << v[i];
		if (i < v.size() - 1)
			std::cout << " ";
	}
	std::cout << "]";
}
