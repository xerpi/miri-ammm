#ifndef SOLVER_GRASP_H
#define SOLVER_GRASP_H

#include <memory>
#include <random>
#include "ISolver.h"
#include "DAT.h"

class Solver_GRASP : ISolver {
public:
	Solver_GRASP(float alpha, uint64_t maxIterations);
	~Solver_GRASP();

	Solution *solve(std::shared_ptr<DAT> dat);

	void setAlpha(float newAlpha);
	float getAlpha();

private:
	Solution *constructivePhase(std::shared_ptr<DAT> dat);

	float alpha;
	uint64_t maxIterations;

	static std::random_device rd;
	static std::mt19937 gen;
};

#endif
