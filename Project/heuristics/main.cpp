#include <iostream>
#include <memory>
#include <chrono>
#include "DAT.h"
#include "Solution.h"
#include "Solver_Greedy.h"
#include "Solver_Greedy_LS.h"
#include "Solver_GRASP.h"

using namespace std;

static void usage(const char *argv)
{
	cout << "Usage:\n\t" << argv << " data.dat" << endl;
}

int main(int argc, char *argv[])
{
	shared_ptr<DAT> dat = make_shared<DAT>();
	chrono::high_resolution_clock::time_point start_time;
	chrono::high_resolution_clock::time_point end_time;
	chrono::duration<double> elapsed_time;

	if (argc < 2) {
		usage(argv[0]);
		return EXIT_FAILURE;
	}

	cout << "Loading \"" << argv[1] << "\" ... ";
	if (!dat->loadFromFile(argv[1])) {
		cout << "Error!" << endl;
		return EXIT_FAILURE;
	} else {
		cout << "OK!" << endl;
	}

	// cout << "Input data:" << endl;
	// dat->print();

	/***  Greedy ****/

	Solver_Greedy greedy;
	Solution *solution_greedy = nullptr;

	cout << "==== Greedy solver ====" << endl;

	start_time = chrono::high_resolution_clock::now();
	solution_greedy = greedy.solve(dat);
	end_time = chrono::high_resolution_clock::now();

	cout << "Greedy returned: " << (solution_greedy != nullptr) << endl;
	if (solution_greedy) {
		cout << "Cost: " << solution_greedy->getCost() << endl;
		//cout << "Solution:" << endl;
		//solution_greedy->print();
	}

	elapsed_time = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time);
	cout << "Time: " << fixed << elapsed_time.count() << endl;

	cout << endl;

	/***  Greedy + LS ****/

	Solver_Greedy_LS greedy_ls;
	Solution *solution_greedy_ls = nullptr;

	cout << "==== Greedy + LS solver ====" << endl;

	start_time = chrono::high_resolution_clock::now();
	solution_greedy_ls = greedy_ls.solve(dat);
	end_time = chrono::high_resolution_clock::now();

	cout << "Greedy + LS returned: " << (solution_greedy_ls != nullptr) << endl;
	if (solution_greedy_ls) {
		cout << "Cost: " << solution_greedy_ls->getCost() << endl;
		//cout << "Solution:" << endl;
		//solution_greedy_ls->print();
	}

	elapsed_time = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time);
	cout << "Time: " << fixed << elapsed_time.count() << endl;

	cout << endl;

	/***  GRASP ****/

	Solver_GRASP grasp(0.1, 5);
	Solution *solution_grasp = nullptr;

	cout << "==== GRASP solver ====" << endl;

	start_time = chrono::high_resolution_clock::now();
	solution_grasp = grasp.solve(dat);
	end_time = chrono::high_resolution_clock::now();

	cout << "GRASP returned: " << (solution_grasp != nullptr) << endl;
	if (solution_grasp) {
		cout << "Cost: " << solution_grasp->getCost() << endl;
		//cout << "Solution:" << endl;
		//solution_grasp->print();
	}

	elapsed_time = chrono::duration_cast<chrono::nanoseconds>(end_time - start_time);
	cout << "Time: " << fixed << elapsed_time.count() << endl;

	if (solution_greedy)
		delete solution_greedy;

	if (solution_greedy_ls)
		delete solution_greedy_ls;

	if (solution_grasp)
		delete solution_grasp;

	return 0;
}
