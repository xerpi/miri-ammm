#ifndef SOLVER_GREEDY_LS_H
#define SOLVER_GREEDY_LS_H

#include <memory>
#include <list>
#include <utility>
#include "Solver_Greedy.h"
#include "Solution.h"
#include "DAT.h"

class Solver_Greedy_LS : Solver_Greedy {
public:
	Solver_Greedy_LS();
	~Solver_Greedy_LS();
	Solution *solve(std::shared_ptr<DAT> dat);
};

#endif
