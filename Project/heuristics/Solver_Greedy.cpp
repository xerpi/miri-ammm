#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>
#include "Solver_Greedy.h"

Solver_Greedy::Solver_Greedy()
{

}

Solver_Greedy::~Solver_Greedy()
{

}

Solution *Solver_Greedy::solve(std::shared_ptr<DAT> dat)
{
	Solution *solution = new Solution(dat);

	while (!solution->isComplete()) {
		// Get full list of candidates
		Solution::candidate_list_t candidates;
		solution->getCandidateList(candidates);
		if (candidates.empty()) {
			delete solution;
			return nullptr;
		}

		// Calculate the cost of the candidates
		std::vector<Solution::candidate_cost_pair_t> candidates_cost;
		solution->calculateCandidatesCost(candidates, candidates_cost);
		// Check for feasible candidates
		if (candidates_cost.empty()) {
			delete solution;
			return nullptr;
		}

		// Sort the list of feasible candidates
		std::sort(candidates_cost.begin(), candidates_cost.end(),
			[&](Solution::candidate_cost_pair_t const& lhs, Solution::candidate_cost_pair_t const& rhs) {
				return lhs.first < rhs.first;
			}
		);

		// Get the candidate with minimum cost
		Solution::candidate_cost_pair_t &best = candidates_cost[0];

		// Add it to the solution
		solution->addCandidateToSolution(*best.second);
	}

	return solution;
}
