#ifndef ISOLVER_H
#define ISOLVER_H

#include <cinttypes>
#include <memory>
#include "Solution.h"
#include "DAT.h"

class ISolver {
public:
	virtual ~ISolver() = default;
	virtual Solution *solve(std::shared_ptr<DAT> dat) = 0;
};

#endif
