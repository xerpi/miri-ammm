/*********************************************
 * AMMM Project OPL Model
 * Authors: Sergi Granell Escalfet
 *          Elisabet Valle Breix
 *********************************************/

// Input data
int wr = ...;
int cost_1 = ...;
int cost_2 = ...;
int cost_3 = ...;
int nProviders = ...;
range P = 1..nProviders;
int available_workers[p in P] = ...;
int cost_contract[p in P] = ...;
int country[p in P] = ...;
int cost_worker[p in P] = ...;

// Decision variables
dvar boolean hired_half[p in P];
dvar boolean hired_full[p in P];
dvar int+ hired_extra[p in P];
dvar int+ hire_taxes[p in P];

// Decision expressions
dexpr int num_hired[p in P] = (available_workers[p] div 2) * hired_half[p] +
                              available_workers[p] * hired_full[p] +
                              hired_extra[p];

dexpr int cost[p in P] = num_hired[p] * cost_worker[p] +
                         (hired_half[p] + hired_full[p]) * cost_contract[p] +
                         hire_taxes[p];

// Objective function
minimize sum(p in P) cost[p];

subject to{
	// Hire exactly wr programmers
	sum(p in P) num_hired[p] == wr;
		
	// For each provider, hire either none, half or full
	forall(p in P)
	  	hired_half[p] + hired_full[p] <= 1;

	// Can't hire extra programmers from a providor if not hiring full and
	// Can't hire more extra programmers than the available
	forall(p in P)
		hired_full[p] * available_workers[p] >= hired_extra[p];
 	
 	// Can't hire programmers from two providers from the same country
	forall(p1 in P, p2 in P: p1 != p2 && country[p1] == country[p2])
		hired_half[p1] + hired_full[p1] + hired_half[p2] + hired_full[p2] <= 1;

	// Taxes
	forall(p in P)
		hire_taxes[p] >= num_hired[p] * cost_1;
		
	forall(p in P)
		hire_taxes[p] >= 5 * cost_1 + (num_hired[p] - 5) * cost_2;
		
	forall(p in P)
		hire_taxes[p] >= 5 * cost_1 + 5 * cost_2 + (num_hired[p] - 10) * cost_3;
}